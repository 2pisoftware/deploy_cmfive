#!/bin/bash

#CONFIG
#SSL_HOST=aws.syntithenai.com
# PREGENERATED SSL CERTS
#SSL_KEY=
#SSL_CERT=
# AWS user credentials to manage certificates and configure load balancer for instance
#AWS_USER=AKIAJVKTBVY7KVVLT3IQ
#AWS_PASS=ZQOrpL3GE6z6qKCJ+a4IPzE1JelvlJu2cg0qiW3b
#AWS_REGION=us-west-2

# INSTALL
# ensure $SSL_HOST DNS is delegated to the instance
## INSTALL CODE SERVER
# ensure proxy server supports ssl and certs folder
# docker run -d -p 80:80 -p 443:443 -v /etc/nginxcerts:/etc/nginx/certs -v /var/run/docker.sock:/tmp/docker.sock:ro jwilder/nginx-proxy
## INSTALL AWS

##RUN
# specify SSL_HOST env var to generate certificate
#
# use a volume mount to share certs with nginx-proxy on code server
# docker run --name myssl -d -P -e VIRTUAL_HOST=myssl.code.2pisoftware.com -e SSL_HOST=myssl.code.2pisoftware.com -v /etc/nginxcerts:/etc/nginxcerts 2pisoftware/cmfive
# 
# set env vars in Dockerfile for AWS params 
# VIRTUAL_HOST=myssl.syntithenai.com 
# SSL_HOST=myssl.syntithenai.com
# AWS_USER=AKIAJVKTBVY7KVVLT3IQ 
# AWS_PASS=ZQOrpL3GE6z6qKCJ+a4IPzE1JelvlJu2cg0qiW3b
# AWS_REGION=us-west-2
#
# SSL_CERT and SSL_KEY can contain the path to pregenerated key and certificate files.
# the files would need to be added using ADD in the Dockerfile or volume mounts at runtime.
# docker run --name STEVE_cmfive -d -P -e VIRTUAL_HOST=aws.code.2pisoftware.com -e SSL_HOST=aws.code.2pisoftware.com -e SSL_CERT=/etc/nginxcerts/aws.code.2pisoftware.com.crt -e SSL_KEY=/etc/nginxcerts/aws.code.2pisoftware.com.key -v /etc/nginxcerts:/etc/nginxcerts steve_cmfive

if [ -n "$SSL_HOST" ]; then
	# AWS EB
	if [ -n "$AWS_USER" -a -n "$AWS_PASS" ]; then
		# ensure awscli toolkit
		pip --version
		if [ "$?" -ne "0" ]; then
				cd /tmp
				curl -O https://bootstrap.pypa.io/get-pip.py
				chmod +x get-pip.py
				./get-pip.py
				pip install awscli
				cd -
		fi
		# write auth details
		mkdir -p /root/.aws/
		echo "[default]" > /root/.aws/credentials
		echo "aws_access_key_id = $AWS_USER" >> /root/.aws/credentials
		echo "aws_secret_access_key = $AWS_PASS" >> /root/.aws/credentials
		echo "[default]" > /root/.aws/config
		echo "region = $AWS_REGION" >> /root/.aws/config
		echo "output = text" >> /root/.aws/config

		# if the key and cert files are specified and exist
		if [ -s "$SSL_CERT" -a -s "$SSL_KEY" ]; then 
			# upload existing cert
			echo "Upload existing certificate $SSL_CERT"
			aws iam upload-server-certificate  --region=$AWS_REGION --server-certificate-name $SSL_HOST  --certificate-body file://$SSL_CERT --private-key file://$SSL_KEY
			echo "Uploaded existing certificate "
		else 
			# use an AWS certificate
			# start by lookup against SSL_HOST for existing certificate
			echo "Try get certificate $SSL_HOST"
			#aws iam get-server-certificate  --region=$AWS_REGION --server-certificate-name $SSL_HOST;
			#if [ "$?" -ne "0" ]; then echo "Request certificate $SSL_HOST"; aws acm request-certificate --region=$REGION --domain-name  $SSL_HOST; fi
			# retrieve certs from volume mount if available
			SSL_CERT=/etc/letsencrypt/live/$SSL_HOST/fullchain.pem
			SSL_KEY=/etc/letsencrypt/live/$SSL_HOST/privkey.pem
			if [ -s "/etc/nginxcerts/$SSL_HOST.cert" -a -s "/etc/nginxcerts/$SSL_HOST.key" ]; then
				cp /etc/nginxcerts/$SSL_HOST.crt  $SSL_CERT;
				cp /etc/nginxcerts/$SSL_HOST.key  $SSL_KEY;
			fi
			HAVE_CERT=
			if [ -s "$SSL_CERT" -a -s "$SSL_KEY" ]; then 
				echo "Already have letsencrypt certificate";
				HAVE_CERT=YES
			else
				letsencrypt certonly --webroot  --webroot-path=/var/www/cmfive --domains=$SSL_HOST --register-unsafely-without-email --agree-tos -n;
				HAVE_CERT=YES
				echo "Got certificate"
			fi
			#cat $SSL_CERT
			echo "Lookup Certificate in AWS"
			certId=`aws iam get-server-certificate  --region=$AWS_REGION --server-certificate-name $SSL_HOST |grep SERVERCERTIFICATEMETADATA | cut -f 2 `
			echo $certId;
			if [ -z "$certId" ]; then
				echo "Upload cert $SSL_CERT    FOR    $SSL_HOST "
				aws iam upload-server-certificate  --region=$AWS_REGION --server-certificate-name $SSL_HOST  --certificate-body file://$SSL_CERT --private-key file://$SSL_KEY
				echo "Uploaded certificate "
			fi
		fi
		echo "Config load balancer for $SSL_HOST";
		# configure load balancer with these credentials
		certId=`aws iam get-server-certificate  --region=$AWS_REGION --server-certificate-name $SSL_HOST |grep SERVERCERTIFICATEMETADATA | cut -f 2 `;
		echo "Got param CERT:$certId   ";
		loadBalancerName=`aws elb describe-load-balancers   --region=$AWS_REGION|grep LOADBALANCERDESCRIPTIONS|cut -f 6`;
		echo "Got param LOADBALANCER_NAME=$loadBalancerName";
		if [ -n "$certId" -a -n "$loadBalancerName" ]; then
			aws elb set-load-balancer-listener-ssl-certificate --region=$AWS_REGION --load-balancer-name $loadBalancerName --load-balancer-port 443 --ssl-certificate-id $certId;
			echo "Done load balancer config";
		else
			echo "Failed load balancer config";
		fi
	# OTHER DEPLOYMENT
	else
		# generate a certficate if required
		if [ -s "$SSL_CERT" -a -s "$SSL_KEY" ]; then 
			# NGINX-PROXY
			# copy certs to volume for nginx-proxy
			if [ -e /etc/nginxcerts ]; then
				cp $SSL_CERT /etc/nginxcerts/$SSL_HOST.crt;
				cp $SSL_KEY /etc/nginxcerts/$SSL_HOST.key;
			fi
		else
			SSL_CERT=/etc/letsencrypt/live/$SSL_HOST/fullchain.pem
			SSL_KEY=/etc/letsencrypt/live/$SSL_HOST/privkey.pem
			letsencrypt certonly --webroot  --webroot-path=/var/www/cmfive --domains=$SSL_HOST --register-unsafely-without-email --agree-tos -n;
			# NGINX-PROXY
			# copy certs to volume for nginx-proxy
			if [ -e /etc/nginxcerts ]; then
				cp $SSL_CERT /etc/nginxcerts/$SSL_HOST.crt;
				cp $SSL_KEY /etc/nginxcerts/$SSL_HOST.key;
			fi
		fi
	fi
	
	# in any case support STANDALONE SSL
	# copy certs to standard location for nginx
	mkdir /etc/nginx/certs
	if [ -s "$SSL_CERT" -a -s "$SSL_KEY" ]; then 
		cp $SSL_CERT /etc/nginx/certs/crt.pem;
		cp $SSL_KEY /etc/nginx/certs/key.pem
		# config nginx with these cred
		cp /etc/nginx/sites-available/default.ssl /etc/nginx/sites-enabled/default
	fi
fi


# clear deployment credentials
rm /root/.aws/credentials
