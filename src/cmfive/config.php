<?php


Config::append("database", array(
    "hostname"  => !empty($_SERVER['RDS_HOSTNAME']) ? $_SERVER['RDS_HOSTNAME'] : 'localhost',
    "username"  => !empty($_SERVER['RDS_USERNAME']) ? $_SERVER['RDS_USERNAME'] : $_SERVER['MYSQL_USER'],
    "password"  => !empty($_SERVER['RDS_PASSWORD']) ? $_SERVER['RDS_PASSWORD'] : $_SERVER['MYSQL_PASS'],
    "database"  => !empty($_SERVER['RDS_DB_NAME']) ?  $_SERVER['RDS_DB_NAME'] : $_SERVER['ON_CREATE_DB'],
    "driver"    => "mysql"
));
// use the API_KEY to authenticate with username and password
Config::append('system', array('rest_api_key' =>"abcdefghijklmnopqrstuv"));

// exclude any objects that you do NOT want available via REST
// note: only DbObjects which have the $_rest; property are 
// accessible via REST anyway!
Config::append('system',array('rest_allow'=>array(
    "User",
    "Contact",
    "WikiPage"
)));
